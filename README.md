#  CSV Uploader 
This is a CSV parsing app that uploads data to a cloud service

## Installation
- Clone the app `git clone https://bitbucket.org/simonmuthusi/csv-uploader-android`
- Open with android studio and compile the app 

## Backend service 
- https://bitbucket.org/simonmuthusi/csv-uploader-backend

## Sample Upload Data
- https://docs.google.com/spreadsheets/d/14JAMjHurboiL16JjPAzaC7gs_q7jlmXBU8JzCrQOK7U/edit#gid=0

## Functionality
- When uploading data, headers will be ignored
- Format data in this format: https://docs.google.com/spreadsheets/d/14JAMjHurboiL16JjPAzaC7gs_q7jlmXBU8JzCrQOK7U/edit?usp=sharing
- Save the file on the device, and click on upload


## Screenshots
###  Main View — Shows listing of uplaoded items
![Listing of Contact details](screenshots/main.png)

### Selecting File to upload.
![Selecting File to upload](screenshots/select-files.png)

### Ask permissions
![Ask permissions](screenshots/permissions.png)
