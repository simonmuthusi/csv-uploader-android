package net.simnmuthusi.csvuploader;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;
import com.nbsp.materialfilepicker.MaterialFilePicker;
import com.nbsp.materialfilepicker.ui.FilePickerActivity;

import net.simnmuthusi.csvuploader.adapters.UploadAdapter;
import net.simnmuthusi.csvuploader.gson.Upload;
import net.simnmuthusi.csvuploader.network.API;
import net.simnmuthusi.csvuploader.network.APIClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Pattern;

import dmax.dialog.SpotsDialog;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    public static final int PERMISSIONS_REQUEST_CODE = 0;
    public static final int FILE_PICKER_REQUEST_CODE = 1;

    public AlertDialog progress;
    private SwipeRefreshLayout swipeContainer;
    private ArrayList<Upload> upload_items;
    private RecyclerView rvUploads;
    private UploadAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                fetch_new_data();
            }
        });
        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);


        rvUploads = (RecyclerView) findViewById(R.id.rvUploads);

        // Initialize contacts
        upload_items = new ArrayList<>();
        adapter = new UploadAdapter(upload_items);
        rvUploads.setAdapter(adapter);
        rvUploads.setLayoutManager(new LinearLayoutManager(this));


        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                checkPermissionsAndOpenFilePicker();
                Snackbar.make(view, "Select your .CSV file", Snackbar.LENGTH_LONG)
                        .setAction("", null).show();

            }
        });

        fetch_new_data();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode,
                                 Intent resultData) {
        super.onActivityResult(requestCode, resultCode, resultData);

        ArrayList<Upload> data_to_upload = new ArrayList<>();


        if (requestCode == FILE_PICKER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {

            progress= new SpotsDialog.Builder().setContext(MainActivity.this).build();

            progress.setMessage("Parsing CSv file");
            progress.show();

            String filePath = resultData.getStringExtra(FilePickerActivity.RESULT_FILE_PATH);

            BufferedReader br = null;
            String line = "";
            String cvsSplitBy = ",";



            try {
                int count = 0;
                br = new BufferedReader(new FileReader(filePath));
                while ((line = br.readLine()) != null) {
                    if(count==0){count++;continue;}

                    // use comma as separator
                    String[] uploads = line.split(cvsSplitBy);
                    String first_name = uploads[0];
                    String last_name = uploads[1];
                    String dob = uploads[2];
                    String postal_address = uploads[3];
                    String national_id = uploads[4];
                    String gender = uploads[5];

                    Upload upload = new Upload(national_id, first_name, last_name, dob, postal_address, gender);
                    data_to_upload.add(upload);

                }

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (br != null) {
                    try {
                        br.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            progress.setMessage("Uploading data ...");
            progress.show();

            upload_data(getApplicationContext(), data_to_upload);


        }
    }

    private void checkPermissionsAndOpenFilePicker() {
        String permission = Manifest.permission.READ_EXTERNAL_STORAGE;

        if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                showError();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{permission}, PERMISSIONS_REQUEST_CODE);
            }
        } else {
            openFilePicker();
        }
    }

    private void showError() {
        Toast.makeText(this, "Allow external storage reading", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_CODE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openFilePicker();
                } else {
                    showError();
                }
            }
        }
    }

    private void openFilePicker() {
        new MaterialFilePicker()
                .withActivity(this)
                .withRequestCode(FILE_PICKER_REQUEST_CODE)
                .withHiddenFiles(true)
                .withFilter(Pattern.compile(".*\\.csv$"))
                .withTitle("Select Valid CSV Format")
                .start();
    }

    public void upload_data(Context context, ArrayList<Upload> data_to_upload){

        API apiService = APIClient.getClient(MainActivity.this).create(API.class);
        Call<ResponseBody> call = apiService.add_upload(data_to_upload);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                String body = response.body().toString();
                Log.e("Data",body);
                fetch_new_data();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

        progress.dismiss();
    }

    public void fetch_new_data() {

        API apiService = APIClient.getClient(MainActivity.this).create(API.class);
        Call<ResponseBody> call = apiService.get_uploads();

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                String body = null;
                try {
                    body = response.body().string();
                    Log.e("TAG", "response 33: "+body  );

                    ArrayList<Upload> new_uploads = new ArrayList<>();
                    Log.e("error", body);
                    JSONObject json = null;
                    try {
                        json = new JSONObject(body);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("error", e.getMessage());
                    }
                    try {
                        JSONArray uItems = json.getJSONArray("data");
                        for(int c=0;c<uItems.length();c++){
                            JSONObject jItem = uItems.getJSONObject(c);

                            String first_name = jItem.getString("first_name");
                            String last_name = jItem.getString("last_name");
                            String dob = jItem.getString("date_of_birth");
                            String postal_address = jItem.getString("postal_address");
                            String national_id = jItem.getString("national_id");
                            String gender = jItem.getString("gender");
                            Upload u = new Upload(national_id, first_name, last_name, dob, postal_address, gender);
                            new_uploads.add(u);
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    adapter.clear();
                    adapter.addAll(new_uploads);
                    adapter.notifyDataSetChanged();
//
                } catch (IOException e) {
                    e.printStackTrace();
                }

                swipeContainer.setRefreshing(false);


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

//        upload_items = new ArrayList<>();
//        UploadAdapter adapter = new UploadAdapter(upload_items);
//        rvUploads.setAdapter(adapter);
//        rvUploads.setLayoutManager(new LinearLayoutManager(this));

//        adapter.clear();
//        // ...the data has come back, add new items to your adapter...
//        adapter.addAll(...);
//        // Now we call setRefreshing(false) to signal refresh has finished
//        swipeContainer.setRefreshing(false);
    }

}
