package net.simnmuthusi.csvuploader.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.simnmuthusi.csvuploader.R;
import net.simnmuthusi.csvuploader.gson.Upload;

import java.util.List;

public class UploadAdapter extends
        RecyclerView.Adapter<UploadAdapter.ViewHolder> {

    private List<Upload> uploads;
    private TextView nameTextView;

    public UploadAdapter(List<Upload> uploads) {
        this.uploads = uploads;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View uploadView = inflater.inflate(R.layout.list_items, parent, false);

        // Return a new holder instance
        ViewHolder viewHolder = new ViewHolder(uploadView);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        Upload upload = uploads.get(i);
        TextView textView = viewHolder.nameTextView;
        TextView textView2 = viewHolder.idGender;
        TextView textView3 = viewHolder.dobPostal;

        textView.setText(upload.getFirst_name() + " "+ upload.getLast_name() + " - "+upload.getNational_id());
        textView2.setText(upload.getDate_of_birth() + " - "+ upload.getGender());
        textView3.setText(upload.getPostal_address());

    }

    @Override
    public int getItemCount() {
        return uploads.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView nameTextView, idGender, dobPostal;

        public ViewHolder(View itemView) {
            super(itemView);

            nameTextView = (TextView) itemView.findViewById(R.id.names);
            idGender = (TextView) itemView.findViewById(R.id.id_gender);
            dobPostal = (TextView) itemView.findViewById(R.id.dob_postal);
        }
    }
    public void clear() {
        uploads.clear();
        notifyDataSetChanged();
    }

    public void addAll(List<Upload> list) {
        uploads.addAll(list);
        notifyDataSetChanged();
    }
}
