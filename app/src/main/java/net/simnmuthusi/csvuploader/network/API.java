package net.simnmuthusi.csvuploader.network;

import net.simnmuthusi.csvuploader.gson.Upload;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface API {

    @POST("/api/v1/uploads/add/")
    Call<ResponseBody> add_upload(@Body ArrayList<Upload> params);

    @GET("/api/v1/uploads/list/")
    Call<ResponseBody> get_uploads();

}